package userMD

import (
	"crypto/sha512"
	"encoding/hex"
)

//CheckPassword 检查密码格式是否正确
func CheckPassword(pwd string) (err string) {
	if len(pwd) < 6 {
		return "密码必须大于6字节!"
	}
	return ""
}

//CheckPassword 检查手机号格式
func CheckPhoneNum(phone string) (err string) {
	if len(phone) != 11 {
		return "请输入11位手机号!"
	}
	return ""
}

//CheckUserToken 检查用户的token是否正确
func CheckUserToken(token string) bool {
	return orm.NewOrm().QueryTable("user").Filter("token", token).Filter("IsStop", false).Exist()
}

//GetUser 获取用户
func GetUser(token string) (*sqlmodel.User, error) {
	u := sqlmodel.User{}
	u.Token = token
	u.IsStop = false
	if err := orm.NewOrm().Read(&u, "Token", "IsStop"); err != nil {
		if err == orm.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}
	return &u, nil
}

var hashSha512 = sha512.New()

func GetUserPassword(token, password string) (ret string) {
	if token == "" {
		panic("请先设置Token")
	}
	if password == "" {
		panic("请先设置password")
	}
	hashSha512.Reset()
	hashSha512.Write([]byte(token))
	hashSha512.Write([]byte(password))
	hashSha512.Write([]byte(token))
	ret = hex.EncodeToString(hashSha512.Sum(nil))
	return
}
