package controllers

import (
	"errors"
	"github.com/astaxie/beego"
	"strings"
	"github.com/astaxie/beego/logs"
	"encoding/json"
	"reflect"
	"strconv"
	"fmt"
	"MoBangServer/models/sqlmodel"
)

var (
	errNotAllowParam = errors.New("NotAllowParam")
)

// Base Class 基础类
type  BaseController struct {
	beego.Controller
	BackInfo  BackInfo
	notJson   bool
}

// Back Infomation返回信息
type BackInfo struct {
	Status  bool
	Message string
	Data    interface{}
}

// Rendering 渲染
func (c *BaseController) Render()  error {
	//api使用json返回
	if !c.notJson && strings.HasPrefix(c.Ctx.Request.URL.Path,"/api"){
		c.infoBack()
		return  nil
	}
	//Other requests use the original route 其他请求使用原来的路由
	return c.Controller.Render()
}

func (c *BaseController) GetInputStruct(in interface{}) (err error){
	c.Ctx.Input.CopyBody(beego.BConfig.MaxMemory)
	logs.Debug(string(c.Ctx.Input.RequestBody))
	if err = json.Unmarshal(c.Ctx.Input.RequestBody,in);
	  err != nil {
		logs.Debug(err)
		c.BackInfo.Message= "Input parameter is illegal！"
		return  errNotAllowParam
	}
	// Check all Tag restrictions 检查所有Tag的限制
	names := reflect.TypeOf(in).Elem()
	values := reflect.ValueOf(in).Elem()
	for  i := 0;i < values.NumField();i++ {
		k := names.Field(i)
		prev := values.Field(i)
		tag := k.Tag
		if prev.Kind() == reflect.Slice {
			return
		}
		if prev.Kind() != reflect.Ptr {
			panic(names.Name() + "." + k.Name + ": Must be pointer type")
		}
		v := prev.Elem()

		// Determine whether or not it is empty 检查是否为空
		_, canEmpty := tag.Lookup("canempty")
		if !canEmpty && prev.IsNil() {
			c.BackInfo.Message = k.Name + "Not empty！"
			return errNotAllowParam
		}
		switch  v.Kind() {
		case reflect.String:
			cnt := v.Len()
			maxlen := int64(255)
			if value, ok := tag.Lookup("maxlen"); ok {
				maxlen, err = strconv.ParseInt(value, 10, 32)
				if err != nil {
					panic(err)
				}
			}
			if int64(cnt) > maxlen {
				c.BackInfo.Message = "Input" + k.Name + "length:" + strconv.Itoa(cnt) + ",Exceeding the maximum limit:" + strconv.Itoa(int(maxlen))
				return errNotAllowParam
			}

		case reflect.Int, reflect.Int8, reflect.Int32, reflect.Int64:
			if value, ok := tag.Lookup("min"); ok {
				min, err := strconv.ParseInt(value, 10, 64)
				if err != nil {
					panic(err)
				}
				if v.Int() < min {
					c.BackInfo.Message = "Input" + k.Name + "length:" + fmt.Sprint("%v", v.Int()) + ",Less than minimum limit:" + fmt.Sprintf("%v", min)
					return errNotAllowParam
				}
			}
			if value, ok := tag.Lookup("max"); ok {
				max, err := strconv.ParseInt(value, 10, 64)
				if err != nil {
					panic(err)
				}
				if v.Int() > max {
					c.BackInfo.Message = "Input" + k.Name + "length:" + fmt.Sprint("%v", v.Int()) + ",Exceeding the maximum limit:" + fmt.Sprintf("%v", max)
					return errNotAllowParam
				}
			}

		case reflect.Uint, reflect.Uint32, reflect.Uint64:
			if value, ok := tag.Lookup("min"); ok {
				min, err := strconv.ParseInt(value, 10, 64)
				if err != nil {
					panic(err)
				}
				if v.Uint() < uint64(min) {
					c.BackInfo.Message = "Input" + k.Name + "value:" + fmt.Sprintf("%v", v.Uint()) + ",Less than minimum limit:" + fmt.Sprintf("%v", min)
					return errNotAllowParam
				}
			}
			if value, ok := tag.Lookup("max"); ok {
				max, err := strconv.ParseInt(value, 10, 64)
				if err != nil {
					panic(err)
				}
				if v.Uint() > uint64(max) {
					c.BackInfo.Message = "Input" + k.Name + "value:" + fmt.Sprintf("%v", v.Uint()) + ",Exceeding the maximum limit:" + fmt.Sprintf("%v", max)
					return errNotAllowParam
				}
			}
		}
	  }
	return
}

func (c *BaseController) SetUserInfo(token string,user *sqlmodel.User){
	c.Data[token] = user
}

func (c *BaseController) GetUserInfo(token string) *sqlmodel.User{
	if c.Data[token] == nil {
		return nil
	}
	return  c.Data[token].(*sqlmodel.User)
}

//Support Cross Domain 支持跨域
func (c *BaseController) AllowOrigin(){
	c.Ctx.Output.Header("Access-Control-Allow-Origin","*")
}


