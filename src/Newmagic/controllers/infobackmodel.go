package controllers

import (
	"MoBangServer/models/sqlmodel"
	"github.com/astaxie/beego/logs"
	"runtime/debug"
	"reflect"
)

type JSONOutPutInterface interface {
	SetBackInfo(Status bool,Message string,Data interface{},err ... error)		//Go语言函数中有三个点...表示为可变参数，可以接受任意个数的参数。
	SetUserInfo(token string,user *sqlmodel.User)
}

//JSONOutPutModel:   For JSON output用于JSON输出
type  jsonOutPutModel struct {
	Status   bool             `json:"status"`
	Message  string           `json:"status"`
	Data     interface{}     `json:"data"`
}

func (this *BaseController) SetBackInfo(Status bool,Message string,Data interface{},err ... error){
	this.BackInfo.Status  = Status
	this.BackInfo.Message = Message
	this.BackInfo.Data    = Data
	if len(err) > 0 {
		this.ErrLog(err[0])
	}
}

func (this *BaseController) ErrLog(err error) {
	logs.Error(err,'\n',"[Input]:",string(this.Ctx.Input.RequestBody),string(debug.Stack()))
}

//InfoBack:  Returns formatting information (no XXS attack vulnerability) 返回格式化信息(无XXS攻击漏洞)
func (this *BaseController) infoBack(){
	this.AllowOrigin()
	m := &jsonOutPutModel{}
	m.Status = this.BackInfo.Status
	m.Message = this.BackInfo.Message
	if nil == this.BackInfo.Data {
		m.Data = ""
	}else if reflect.TypeOf(this.BackInfo.Data).Kind() == reflect.Slice && reflect.ValueOf(this.BackInfo.Data).Len() == 0 {
		//One level empty array 一级空数组
		m.Data = make([]interface{}, 0)
	}else {
		m.Data = this.BackInfo.Data
	}

	//OutPut JSON
	this.Data["json"] = m
	this.ServeJSON()
}