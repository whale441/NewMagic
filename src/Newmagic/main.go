package main

import (
	"flag"
	"os"
	"os/signal"
	"syscall"
)

var (
	creatRouters = flag.Bool("CR", false, "是否只是生成routers文件(一般用于自动部署的)")
)

//main
func main() {
	flag.Parse()
	if *creatRouters {
		return
	}
	start.MysqlInit()
	//beego.ErrorHandler("404", models.Page404) //修改404页面
	start.SetFilter()
	start.CheckLogWrite()
	//收到关闭信号后等待关闭所有连接
	exit := make(chan struct{})
	go func() {
		signalChan := make(chan os.Signal, 1)
		signal.Notify(signalChan,
			syscall.SIGHUP,
			syscall.SIGINT,
			syscall.SIGTERM,
			syscall.SIGQUIT)
		<-signalChan
		start.ShutDown()
		exit <- struct{}{}
	}()
	beego.Run()
	<-exit
}

