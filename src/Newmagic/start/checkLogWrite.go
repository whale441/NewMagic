package start

import (
	"os"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/logs"
)

type  EmptyWriter struct {

}

func (*EmptyWriter) Write(p []byte) (n int,err error){
	return 0,nil
}

func CheckLogWrite() {
	os.Mkdir("static/upload",os.ModePerm)
	beego.BeeLogger.Async(le3)

	//Online mode custom log output to file, or else output to console/线上模式自定义日志输出到文件，否则输出到控制台
	if beego.AppConfig.String("runmode") == "prod"{
		os.Mkdir("log",os.ModePerm)
		err := logs.SetLogger(logs.AdapterFile,`{"filename":"log/logfile","daily":false}`)
		if err != nil {
			panic(err)
		}
		beego.BeeLogger.DelLogger("console")
		logs.SetLevel(logs.LevelInfo)
	}
}
