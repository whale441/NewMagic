package start


func MysqlInit() {
	DB_USERNAME := beego.AppConfig.String("DB_USERNAME")
	DB_PASSWORD := beego.AppConfig.String("DB_PASSWORD")
	DB_IP := beego.AppConfig.String("DB_IP")
	DB_PORT := beego.AppConfig.String("DB_PORT")
	DB_NAME := beego.AppConfig.String("DB_NAME")
	DB_MaxOpenConns, err := beego.AppConfig.Int("DB_MaxOpenConns")
	if err != nil {
		panic("DB_MaxOpenConns错误："+ err.Error())
	}
}