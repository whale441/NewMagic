package start

import (
	"flag"
	"github.com/astaxie/beego"
	"context"
)

var(
	//是否开启代理服务(影响ip获取)
	EnableX_FORWARDED = flag.Bool("EnableX_FORWARDED", false,"Whether to open proxy service (affecting IP access)")
)

// 设置过滤器/catalog filter
func setFilter() {
	EnableHttps, err  := beego.AppConfig.Bool("EnableHttps")
	if err != nil {
		panic(err)
	}
	//自定义的路由规则(检查是否需要渲染,需要渲染对应内部URL加上"view","static"则不用渲染,对外都直接是根目录)
	// /api不加前缀


}